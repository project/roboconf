
-- SUMMARY --

The Roboconf module aids in theme development by allowing the user to switch between different preset block configurations and theme settings. It is a quick and easy way to test a theme with blocks in different regions and different theme elements activated. The module offers a block that allows users to switch configurations on the fly, a random setting that changes configurations upon page load, as well as urls that facilitate cross-browser testing.

Roboconf was developed by Zivtech for Top Notch Themes.

For a full description visit the project page:
  http://drupal.org/project/roboconf
Bug reports, feature suggestions and latest developments:
  http://drupal.org/project/issues/roboconf


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

* Enable the module in Administer >> Modules.


-- CONFIGURATION --

* Configure user permissions in Administer >> User management >> Permissions
  
  Enable the user roles authorized to test with Roboconf.

* Customize the settings in Administer >> Site configuration >> Roboconf to add block and theme presets.

* Go to Administer >> Site building >> Blocks and enable the "Roboconf Preset Switcher". 


-- NOTES --

This module allows switching between settings of a selected theme. To switch between different themes see: http://drupal.org/project/switchtheme


-- USAGE --

To create a block preset:

* Go to Administer >> Site configuration >> Roboconf and click 'Add Preset'.

* Add Human-Readable and Machine-Readable names and select 'Block Configuration'. Selecting 'Use current configuration' will make the preset use the current blocks in the current regions.

* Go to Administer >> Site building >> Blocks and move the desired blocks into the desired regions for the preset which was just created.

* Go to Administer >> Site configuration >> Roboconf and click 'view' for the preset which was just created. Click 'Update preset to current block configuration'. 

Example: To create a preset with only blocks in the 'Left sidebar' region create a preset 'Left Sidebar Only' (machine readable 'left_only). Go to the blocks page and move all of the desired blocks to the 'Left sidebar' region.  Return to the Roboconf page and click 'view' next to 'Left Sidebar Only' preset. Click 'Update preset to current block configuration'. 

To create a theme preset: 

* Follow the steps above, selecting 'Theme Settings' instead of 'Block Configuration'. Go to Administer >> Site building >> Themes and select desired theme settings for that preset. Go to the settings page for that preset and click 'Update preset to current block configuration'. 


-- TESTING --

Use the "Roboconf Preset Switcher" to switch between different preset block configuration and theme settings.

Switch between presets on the Roboconf settings page by clicking 'activate' on the row of the desired preset. 

Use the urls from specific presets found in the 'Usage' column to test in different browsers or using screen capture software.


-- CONTACT --

Developer:
* Jody Hamilton (Lynn) - jody@zivtech.com

Maintainers:
* Alex Urevick-Ackelsberg (Alex UA) - alex@zivtech.com
* Stephanie Pakrul (stephthegeek) - http://drupal.org/user/47874
* Aaron Couch (acouch) - aaron@zivtech.com


This project has been sponsored by:
* TopNotch Themes
  Gorgeous, advanced Drupal 5 and 6 themes, designed by Drupal theme experts. Visit http://www.topnotchthemes.com for more information.

