<?php

/**
 * @file
 * Administrative page callbacks for the roboconf module.
 */

/**
 * List all current presets with links to edit and administer them.
 */
function roboconf_admin() {
  // List of preset block configurations.
  $current_preset = roboconf_current_preset('blocks');
  $current = $current_preset['pid'];
  $random = $current_preset['random'];
  $result = db_query("SELECT * FROM {roboconf_presets} WHERE type = 'blocks' ORDER BY fullname ASC");
  $rows = array();
  
  $header = array(
    array('data' => t('Name')),
    array('data' => t('Operations'), 'colspan' => '4'),
    array('data' => t('Link')),
    array('data' => t('Usage')),
  );
  //the default preset row
  $rows[] = array(
    t('default'),
    array('data' => t('The current block placement.'), 'colspan' => '4'),
    $current == 0 && !$random ? '<b>'. t('active') .'</b>' : l('activate', 'admin/settings/roboconf', array('query' => 'blocks=default')),
    '<em>'. t('?blocks=default') .'</em>', 
    );
    
  // the random preset row
  $rows[] = array(
    t('random'),
    array('data' => t('A random preset on each page load.'), 'colspan' => '4'),
    $current_preset['random'] ? '<b>'. t('active') .'</b>' : l('activate', 'admin/settings/roboconf', array('query' => 'blocks=random')),
    '<em>'. t('?blocks=random') .'</em>',
    );
    
  // user-defined preset rows
  while ($preset = db_fetch_object($result)) {
    $rows[] = array(
      $preset->fullname,
      l('view', 'admin/settings/roboconf/blocks/'. $preset->pid),
      l('edit', 'admin/settings/roboconf/edit/'. $preset->pid),
      l('delete', 'admin/settings/roboconf/delete/'. $preset->pid),
      l('clone', 'admin/settings/roboconf/clone/'. $preset->pid),
      $current == $preset->pid && !$random ? '<b>'. t('active') .'</b>' : l('activate', 'admin/settings/roboconf', array('query' => 'blocks='. $preset->name)),
      '<em>'. t('?blocks='. $preset->name) .'</em>',
    );
  }
  $output = '<h3>' . t('Preset Block Configurations:') . '</h3>';
  $output .= theme('table', $header, $rows);
  
  if (count($rows) < 3) {
    $output .= t('No presets block configurations have been defined.  You can <a href="@add-page">add</a> a new preset.', array('@add-page' => url('admin/settings/roboconf/add')));
  }
  
  $output .= '<div class="description">'. t('Your current block configuration preset is %preset', array('%preset' => $current_preset['fullname'])) .'.</div>';
  
  
  // List of preset theme settings.
  $current_preset = roboconf_current_preset('settings');
  $current = $current_preset['pid'];
  $random = $current_preset['random'];
  $result = db_query("SELECT * FROM {roboconf_presets} WHERE type = 'settings' ORDER BY fullname ASC");
  $rows = array();
  
  $header = array(
    array('data' => t('Name')),
    array('data' => t('Operations'), 'colspan' => '4'),
    array('data' => t('Link')),
    array('data' => t('Usage')),
  );
  //the default preset row
  $rows[] = array(
    t('default'),
    array('data' => t('The current theme settings.'), 'colspan' => '4'),
    $current == 0 && !$random ? '<b>'. t('active') .'</b>' : l('activate', 'admin/settings/roboconf', array('query' => 'settings=default')),
    '<em>'. t('?settings=default') .'</em>', 
    );
    
  // the random preset row
  $rows[] = array(
    t('random'),
    array('data' => t('A random preset on each page load.'), 'colspan' => '4'),
    $current_preset['random'] ? '<b>'. t('active') .'</b>' : l('activate', 'admin/settings/roboconf', array('query' => 'settings=random')),
    '<em>'. t('?settings=random') .'</em>',
    );
    
  // user-defined preset rows
  while ($preset = db_fetch_object($result)) {
    $rows[] = array(
      $preset->fullname,
      l('view', 'admin/settings/roboconf/settings/'. $preset->pid),
      l('edit', 'admin/settings/roboconf/edit/'. $preset->pid),
      l('delete', 'admin/settings/roboconf/delete/'. $preset->pid),
      l('clone', 'admin/settings/roboconf/clone/'. $preset->pid),
      $current == $preset->pid && !$random ? '<b>'. t('active') .'</b>' : l('activate', 'admin/settings/roboconf', array('query' => 'settings='. $preset->name)),
      '<em>'. t('?settings='. $preset->name) .'</em>',
    );
  }
  $output .= '<br/><br/><h3>' . t('Preset Theme Settings:') . '</h3>';
  $output .= theme('table', $header, $rows);
  
  if (count($rows) < 3) {
    $output .= t('No preset theme settings have been defined.  You can <a href="@add-page">add</a> a new preset.', array('@add-page' => url('admin/settings/roboconf/add')));
  }
  
  $output .= '<div class="description">'. t('Your current theme settings preset is %preset', array('%preset' => $current_preset['fullname'])) .'.</div>';
  
  return $output;
}

/**
 * Menu callback; handles pages for creating and editing themer presets.
 */
function roboconf_admin_edit($pid = 0) {
  if ($pid) {
    $preset = db_fetch_object(db_query("SELECT * FROM {roboconf_presets} WHERE pid = %d", $pid));
    $output = drupal_get_form('roboconf_admin_form', array('pid' => $pid, 'name' => $preset->name, 'fullname' => $preset->fullname, 'type' => $preset->type));
  }
  else {
    $output = drupal_get_form('roboconf_admin_form');
  }

  return $output;
}

function roboconf_admin_clone($pid = 0) {
  if ($pid) {
    $output = drupal_get_form('roboconf_admin_clone_form', $pid);
  }
  else {
    $output = "Invalid preset ID.";
  }
  return $output;
}

function roboconf_admin_clone_form(&$form_state, $pid) {
  $preset = db_fetch_object(db_query("SELECT * FROM {roboconf_presets} WHERE pid = %d", $pid));
  drupal_set_title(t('Clone %name', array('%name' => $preset->fullname)));
  $rows = array();
  $header = array(
    array('data' => t('Field')),
    array('data' => t('Value')),
  );
  
  $rows[] = array(t('Name'), $preset->fullname);
  $rows[] = array(t('Machine Readable Name'), $preset->name);
  $rows[] = array(t('Type'), $preset->type);
  $output .= theme('table', $header, $rows);
  $form['info'] = array(
    '#type' => 'markup',
    '#value' => $output,
  );
  
  $form['title'] = array(
    '#type' => 'markup',
    '#value' => "<h2>" . t('Clone this preset to:') . "</h2>",
  );
  $form['fullname'] = array(
    '#type' => 'textfield',
    '#title' => t('Human-Readable Name'),
    '#default_value' => $edit['fullname'],
    '#maxlength' => 255,
    '#size' => 45,
    '#description' => t('Specify a human-readable preset name. Not necessary if copying to default.'),
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Machine-Readable Name'),
    '#default_value' => $edit['name'],
    '#maxlength' => 64,
    '#size' => 45,
    '#description' => t('Specify a machine-readable preset name. Not necessary if copying to default.'),
  );
  $form['type'] = array(
    '#type' => 'radios',
    '#title' => t('Type'),
    '#description' => t('Specify a type of preset: Blocks configuration or theme settings.'),
    '#required' => TRUE,
    '#options' => array('blocks' => t('Block Configuration'), 'settings' => t('Theme Settings')),
    '#default_value' => $preset->type,
    '#disabled' => TRUE,
  );
 
  //$form['#validate'][] = 'roboconf_admin_form_validate';
  $form['pid'] = array('#type' => 'value', '#value' => $pid);
  $form['submit'] = array('#type' => 'submit', '#validate' => array('roboconf_admin_form_validate'), '#value' => t('Clone to New'));
  $form['copy'] = array('#type' => 'submit', '#value' => t('Copy to Default'));
  
  return $form;
}

function roboconf_admin_clone_form_submit($form, &$form_state) {
  //print "<pre>" . Print_r($form_state, TRUE) . "</pre>";
  
  $pid = $form_state['values']['pid'];
  if ($form_state['values']['op'] == 'Clone to New') {
    db_query("INSERT INTO {roboconf_presets} (name, fullname, type) VALUES ('%s', '%s', '%s')", $form_state['values']['name'], $form_state['values']['fullname'], $form_state['values']['type']);
    $newpid = db_last_insert_id('roboconf_presets', 'pid');
    
    if ($form_state['values']['type'] == 'blocks') {
      $current_theme = variable_get('theme_default', 'garland');
      $result = db_query("SELECT * FROM {roboconf_blocks} WHERE pid = %d", $pid);
      while ($block = db_fetch_object($result)) {
        db_query("INSERT INTO {roboconf_blocks} (pid, module, delta, region, weight) VALUES (%d, '%s', '%s', '%s', %d)", $newpid, $block->module, $block->delta, $block->region, $block->weight);
      } 
    }
    else if ($form_state['values']['type'] == 'settings') {
      $preset = db_fetch_object(db_query("SELECT * FROM {roboconf_settings} WHERE pid = %d", $pid));
      db_query("INSERT INTO {roboconf_settings} (pid, settings, site_info, permissions) VALUES (%d, '%s', '%s', '%s')", $newpid, $preset->settings, $preset->site_info, $preset->permissions);
    }
  
    drupal_set_message(t('The preset has been cloned.'));
  }
  else if ($form_state['values']['op'] == 'Copy to Default') {
    if ($form_state['values']['type'] == 'blocks') {
      //global $theme_key;
      //if (!isset($theme_key)) {
      //  init_theme();
      //}
      db_query("UPDATE {blocks} SET status = %d WHERE status = %d", 0, 1);
      $result = db_query("SELECT * FROM {roboconf_blocks} WHERE pid = %d", $pid);
      while ($block = db_fetch_object($result)) {
        //db_query("UPDATE {blocks} SET status = %d, weight = '%s', region = '%s' WHERE theme = '%s' AND module = '%s' AND delta = '%s'", 1, $block->weight, $block->region, $theme_key, $block->module, $block->delta);
        db_query("UPDATE {blocks} SET status = %d, weight = '%s', region = '%s' WHERE module = '%s' AND delta = '%s'", 1, $block->weight, $block->region, $block->module, $block->delta);
      }
    }
    else if ($form_state['values']['type'] == 'settings') {
      global $theme_key;
      if (!isset($theme_key)) {
        init_theme();
      }
      $result = db_fetch_array(db_query("SELECT settings, site_info, permissions FROM {roboconf_settings} WHERE pid = %d", $pid));
      $settings = unserialize($result['settings']);
      $site_info = unserialize($result['site_info']);
      $preset_perms = unserialize($result['permissions']);
      //print "<pre>" . Print_r($settings, TRUE) . "</pre>";
      //print "<pre>" . Print_r($site_info, TRUE) . "</pre>";
      //print "<pre>" . Print_r($preset_perms, TRUE) . "</pre>";
      
      variable_set('theme_settings', $settings);
      variable_set('theme_' . $theme_key .'_settings', $settings);
      
      if (isset($site_info['site_name'])) { variable_set('site_name', $site_info['site_name']); }
      if (isset($site_info['site_mail'])) { variable_set('site_mail', $site_info['site_mail']); }
      if (isset($site_info['site_slogan'])) { variable_set('site_slogan', $site_info['site_slogan']); }
      if (isset($site_info['site_mission'])) { variable_set('site_mission', $site_info['site_mission']); }
      if (isset($site_info['site_footer'])) { variable_set('site_footer', $site_info['site_footer']); }
      if (isset($site_info['date_default_timezone'])) { variable_set('date_default_timezone', $site_info['date_default_timezone']); }
      if (isset($site_info['configurable_timezones'])) { variable_set('configurable_timezones', $site_info['configurable_timezones']); }
      if (isset($site_info['date_first_day'])) { variable_set('date_first_day', $site_info['date_first_day']); }
      if (isset($site_info['date_format_short'])) { variable_set('date_format_short', $site_info['date_format_short']); }
      if (isset($site_info['date_format_short_custom'])) { variable_set('date_format_short_custom', $site_info['date_format_short_custom']); }
      if (isset($site_info['date_format_medium'])) { variable_set('date_format_medium', $site_info['date_format_medium']); }
      if (isset($site_info['date_format_medium_custom'])) { variable_set('date_format_medium_custom', $site_info['date_format_medium_custom']); }
      if (isset($site_info['date_format_long'])) { variable_set('date_format_long', $site_info['date_format_long']); }
      if (isset($site_info['date_format_long_custom'])) { variable_set('date_format_long_custom', $site_info['date_format_long_custom']); }
      if (isset($site_info['anonymous'])) { variable_set('anonymous', $site_info['anonymous']); }
      if (isset($site_info['site_frontpage'])) { variable_set('site_frontpage', $site_info['site_frontpage']); }
      
      variable_set('roboconf_default_permissions', $preset_perms);
    }
    drupal_set_message(t('The preset has been copied to the default settings.'));
  }
  $form_state['redirect'] = 'admin/settings/roboconf';
  return;
}

/**
 * Return a form for editing or creating an individual themer preset.
 *
 * @ingroup forms
 * @see roboconf_admin_form_validate()
 * @see roboconf_admin_form_submit()
 */
function roboconf_admin_form(&$form_state, $edit = array('pid' => NULL, 'name' => '', 'fullname' => '', 'type' => '')) {
  $form['#preset'] = $edit;
  $form['fullname'] = array(
    '#type' => 'textfield',
    '#title' => t('Human-Readable Name'),
    '#default_value' => $edit['fullname'],
    '#maxlength' => 255,
    '#size' => 45,
    '#description' => t('Specify a human-readable preset name.'),
    '#required' => TRUE,
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Machine-Readable Name'),
    '#default_value' => $edit['name'],
    '#maxlength' => 64,
    '#size' => 45,
    '#description' => t('Specify a machine-readable preset name.'),
    '#required' => TRUE,
  );
  $form['type'] = array(
    '#type' => 'radios',
    '#title' => t('Type'),
    '#description' => t('Specify a type of preset: Blocks configuration or theme settings.'),
    '#required' => TRUE,
    '#options' => array('blocks' => t('Block Configuration'), 'settings' => t('Theme Settings')),
    '#default_value' => $edit['type'],
  );
  $form['current'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use current configuration.'),
    '#default_value' => !$edit['type'],
    '#description' => t('Checking this box will save this preset with either the current block configuration or current theme settings, depending on the type.'),
    '#required' => TRUE,
  );
  
  if ($edit['pid']) {
    $form['pid'] = array('#type' => 'hidden', '#value' => $edit['pid']);
    $form['submit'] = array('#type' => 'submit', '#value' => t('Update preset'));
  }
  else {
    $form['submit'] = array('#type' => 'submit', '#value' => t('Create new preset'));
  }
  $form['markup'] = array(
    '#value' => '<div class="description">The current configuration will be used for this preset. The preset may be modified later.</div>',
  );

  return $form;
}

/**
 * Verify that the preset name is machine-readable and unique
 */
function roboconf_admin_form_validate($form, &$form_state) {
  $name = $form_state['values']['name'];
  $fullname = $form_state['values']['fullname'];
  $pid = isset($form_state['values']['pid']) ? $form_state['values']['pid'] : 0;

  if ($name == '') {
    form_set_error('name', t('You must enter a machine-readable name.'));
  }
  if ($fullname == '') {
    form_set_error('fullname', t('You must enter a human-readable name.'));
  }
  if (db_result(db_query("SELECT COUNT(name) FROM {roboconf_presets} WHERE pid != %d AND name = '%s'", $pid, $name))) {
    form_set_error('name', t('The machine-readable name %name is already in use.  Please choose a unique name.', array('%name' => $name)));
  }
  if (db_result(db_query("SELECT COUNT(fullname) FROM {roboconf_presets} WHERE pid != %d AND fullname = '%s'", $pid, $fullname))) {
    form_set_error('fullname', t('The preset name %name is already in use.  Please choose a unique name.', array('%name' => $fullname)));
  }
  if (!preg_match('!^[a-z0-9_]+$!', $name)) {
    form_set_error('field_name', t('The machine-readable name %name is invalid. The name must include only lowercase unaccentuated letters, numbers, and underscores.', array('%name' => $name)));
  }
  if ($name == 'default') {
    form_set_error('name', t('You cannot use the name "default" as it is used by the system.'));
  }
  if ($name == 'random') {
    form_set_error('name', t('You cannot use the name "random" as it is used by the system.'));
  }
  if ($fullname == 'default') {
    form_set_error('fullname', t('You cannot use the name "default" as it is used by the system.'));
  }
  if ($fullname == 'random') {
    form_set_error('fullname', t('You cannot use the name "random" as it is used by the system.'));
  }
}

/**
 * Save a new themer preset to the database.
 */
function roboconf_admin_form_submit($form, &$form_state) {
  if ($form_state['values']['pid']) {
    db_query("UPDATE {roboconf_presets} SET name = '%s', fullname = '%s', type = '%s' WHERE pid = %d", $form_state['values']['name'], $form_state['values']['fullname'],$form_state['values']['type'], $form_state['values']['pid']);
    roboconf_copy_data($form_state['values']['pid'], $form_state['values']['type']);
  }
  else {
    db_query("INSERT INTO {roboconf_presets} (name, fullname, type) VALUES ('%s', '%s', '%s')", $form_state['values']['name'], $form_state['values']['fullname'], $form_state['values']['type']);
    roboconf_copy_data(db_last_insert_id('roboconf_presets', 'pid'), $form_state['values']['type']);
  }

  drupal_set_message(t('The preset has been saved.'));
  $form_state['redirect'] = 'admin/settings/roboconf';
  return;
}

/**
 * Menu callback; confirms deleting a themer preset.
 */
function roboconf_admin_delete_confirm($form_state, $pid) {
  $fullname = db_result(db_query("SELECT fullname FROM {roboconf_presets} WHERE pid = %d", $pid));
  if (user_access('administer roboconf')) {
    $form['pid'] = array('#type' => 'value', '#value' => $pid);
    $output = confirm_form($form,
      t('Are you sure you want to delete the preset, %name?', array('%name' => $fullname)),
      isset($_GET['destination']) ? $_GET['destination'] : 'admin/settings/roboconf');
  }
  return $output;
}

/**
 * Execute themer preset deletion.
 */
function roboconf_admin_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    db_query("DELETE FROM {roboconf_presets} WHERE pid = %d", $form_state['values']['pid']);
    db_query("DELETE FROM {roboconf_blocks} WHERE pid = %d", $form_state['values']['pid']);
    db_query("DELETE FROM {roboconf_settings} WHERE pid = %d", $form_state['values']['pid']);
    $form_state['redirect'] = 'admin/settings/roboconf';
    return;
  }
}

/**
 * Menu callback; handles pages for setting block placements for preset.
 */
function roboconf_admin_blocks($pid) {
  $fullname = db_result(db_query("SELECT fullname FROM {roboconf_presets} WHERE pid = %d", $pid));
  drupal_set_title(t('%name Preset Block Placement', array('%name' => $fullname)));
  $result = db_query("SELECT * FROM {roboconf_blocks} WHERE pid = %d ORDER BY region ASC, weight ASC", $pid);
  $set_rows = array();
  $set_blocks = array();
  while ($block = db_fetch_object($result)) {
    $set_blocks[$block->module][$block->delta] = $block;
  }
  $current_theme = variable_get('theme_default', 'garland');
  $result = db_query("SELECT * FROM {blocks} WHERE theme = '%s' AND status = 1 ORDER BY region ASC, weight ASC", $current_theme);
  $rows = array();
  $blocks = array();
  while ($block = db_fetch_object($result)) {
    $blocks[$block->module][$block->delta] = $block;
  }
  foreach (module_list() as $module) {
    $module_blocks = module_invoke($module, 'block', 'list');
    if ($module_blocks) {
      foreach ($module_blocks as $delta => $block) {
        if ($blocks[$module][$delta]) {
          $key = $blocks[$module][$delta]->region;
          $key .= sprintf('%05d', $blocks[$module][$delta]->weight + 100) . $module . $delta;
          $rows[$key] = array(
            $block['info'],
            $module,
            $delta,
            $blocks[$module][$delta]->region,
            $blocks[$module][$delta]->weight,
          ); 
        }
        if ($set_blocks[$module][$delta]) {
          $key = $set_blocks[$module][$delta]->region;
          $key .= sprintf('%05d', $set_blocks[$module][$delta]->weight + 100) . $module . $delta;
          $set_rows[$key] = array(
            $block['info'],
            $module,
            $delta,
            $set_blocks[$module][$delta]->region,
            $set_blocks[$module][$delta]->weight,
          );
        }       
      }
    }
  }
  ksort($rows);
  ksort($set_rows);
  $header = array('name', 'module', 'delta', 'region', 'weight');
  if (count($set_rows)) {
    $output .= theme('table', $header, $set_rows, array(), t('Block Settings for Preset %preset', array('%preset' => $fullname)));
    $output .= t('Submitting this form will override these settings with the current settings below.');
  }
  else {
    $output .= t('No blocks have been set for this preset.  You can update the preset to the current block configuration as is or first <a href="@block-url">set the block placement</a>.', array('@block-url' => url('admin/build/block')));
  }
  $output .= drupal_get_form('roboconf_block_form', array('pid' => $pid));
  $output .= theme('table', $header, $rows, array(), t('Current Block Settings (from current theme %theme)', array('%theme' => $current_theme)));

  return $output;
}

/**
 * Return a form for editing or creating an individual themer preset.
 *
 * @ingroup forms
 * @see roboconf_block_form_submit()
 */
function roboconf_block_form(&$form_state, $edit = array('pid' => NULL)) {
  $form['pid'] = array('#type' => 'hidden', '#value' => $edit['pid']);
  $form['submit'] = array('#type' => 'submit', '#value' => t('Update preset to current block configuration'));
  return $form;
}

/**
 * Save a block configuration as a preset.
 */
function roboconf_block_form_submit($form, &$form_state) {
  roboconf_copy_data($form_state['values']['pid'], 'blocks');
  $form_state['redirect'] = 'admin/settings/roboconf';
  return;
}

/**
 * Copy current settings into a preset.
 */
function roboconf_copy_data($pid, $type) {
  switch ($type) {
      case 'blocks':
        roboconf_copy_blocks($pid);
        break;
      case 'settings':
        roboconf_copy_settings($pid);
        break;
  }
}

/**
 * Copies the site's current block configuration into a block preset. 
 */
function roboconf_copy_blocks($pid) {
  db_query("DELETE FROM {roboconf_blocks} WHERE pid = %d", $pid);
  $current_theme = variable_get('theme_default', 'garland');
  $result = db_query("SELECT * FROM {blocks} WHERE theme = '%s' AND status = 1", $current_theme);
  while ($block = db_fetch_object($result)) {
    db_query("INSERT INTO {roboconf_blocks} (pid, module, delta, region, weight) VALUES (%d, '%s', '%s', '%s', %d)", $pid, $block->module, $block->delta, $block->region, $block->weight);
  } 
  return;
}

/**
 * View theme setting preset.
 */
function roboconf_admin_settings($pid) {
  $fullname = db_result(db_query("SELECT fullname FROM {roboconf_presets} WHERE pid = %d", $pid));
  drupal_set_title(t('%name Preset Theme Settings', array('%name' => $fullname)));
  $preset_settings = unserialize(db_result(db_query("SELECT settings FROM {roboconf_settings} WHERE pid = %d", $pid)));
  $current_settings = roboconf_stored_settings();

  $output .= t('Submitting this form will override these settings with the current default settings.');
  
  $output .= drupal_get_form('roboconf_settings_form', array('pid' => $pid));
  
  $rows = array();
  $header = array(
    array('data' => t('Settings')),
    array('data' => t('Value')),
  );
    
  foreach ($preset_settings as $name => $value) {
    if (is_array($value)) {
      $value = "<pre>" . Print_r($value, TRUE) . "</pre>";
    }
    $rows[] = array(
      $name,
      $value,
    );
  }
  $output .= '<h3>' . t('Preset Theme Settings:') . '</h3>';
  $output .= theme('table', $header, $rows);
  
  if (count($rows) < 1) {
    $output .= t('There was an error loading the theme settings.');
  }
  
  
  $rows = array();
  $header = array(
    array('data' => t('Settings')),
    array('data' => t('Value')),
  );
  
  foreach ($current_settings as $name => $value) {
    if (is_array($value)) {
      $value = "<pre>" . Print_r($value, TRUE) . "</pre>";
    }
    $rows[] = array($name,$value);
  }
  $output .= '<h3>' . t('Current Theme Settings:') . '</h3>';
  $output .= theme('table', $header, $rows);
  
  if (count($rows) < 1) {
    $output .= t('There was an error loading the theme settings.');
  }
  
  return $output;
}

/**
 * Return a form for editing or creating an individual theme settings preset.
 *
 * @ingroup forms
 * @see roboconf_settings_form_submit()
 */
function roboconf_settings_form(&$form_state, $edit = array('pid' => NULL)) {
  $form['pid'] = array('#type' => 'hidden', '#value' => $edit['pid']);
  $form['submit'] = array('#type' => 'submit', '#value' => t('Update preset to current theme settings'));
  return $form;
}

/**
 * Save a theme settings as a preset.
 */
function roboconf_settings_form_submit($form, &$form_state) {
  roboconf_copy_data($form_state['values']['pid'], 'settings');
  $form_state['redirect'] = 'admin/settings/roboconf';
  return;
}

/**
 * Copy the current theme settings into a preset.
 */
function roboconf_copy_settings($pid) {
  global $theme_key;
  if (!isset($theme_key)) {
    init_theme();
  }
  db_query("DELETE FROM {roboconf_settings} WHERE pid = %d", $pid);

  $theme_settings = roboconf_stored_settings();
  $info = roboconf_stored_info();
  $permissions = variable_get('roboconf_default_permissions', array());
  
  db_query("INSERT INTO {roboconf_settings} (pid, settings, site_info, permissions) VALUES (%d, '%s', '%s', '%s')", $pid, serialize($theme_settings), serialize($info), serialize($permissions));
  variable_set('theme_settings', $theme_settings);
  return;
}
